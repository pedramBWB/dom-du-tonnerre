<?php 

namespace Beweb\Td\Models\Impl;

use Job;
use Race;

class Dwarf extends Race {
    // modificateurs :
        function __construct(){
            parent::__construct();
            $this->modifiers->pv = 50;
            $this->modifiers->att = 2;
            $this->modifiers->def = 25;
        }
}

?>