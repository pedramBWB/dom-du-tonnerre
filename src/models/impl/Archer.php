<?php 

namespace Beweb\Td\Models\Impl;

use Job;
use Race;

class Archer extends Job {
    // modificateurs :
    function getModifPv(): int {
        return 150;
    }

    function getModifAtt(): int{
        return 20;
    }

    function getModifDef(): int{
        return 20;
    }
}

?>