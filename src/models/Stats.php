<?php 

class Stats {
    private int $hp; // Points de Vie, set à 50 (différents en fonction de la race et du job)
    private int $atk; // Attaque, set à 100 (différente en fonction de la race et du job)
    private int $def; // Défense, set à 30 (différente en fonction de la race et du job)
    private int $cRate; // Taux Critique, set à 15% (différent selon le job)
    private int $cDmg; // Dégât Critique, set à 10% de l'atk (différent selon le job)

    // Création de mon objet
    function __construct(){
        $this->pv = 0;
        $this->def = 0;
        $this->att = 0;
    }
}

?>