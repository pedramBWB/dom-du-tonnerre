<?php

namespace Beweb\Td\Models;

use Job;
use Race;
use Stats;
use Beweb\Td\Models\Interfaces\Fighter;

class Character implements Fighter {
    private Race $race; // Choisir sa race
    private Job $job; // Choisir son job
    private Stats $stats; // Récupérer les stats en fonction de la race et du job

    public function __construct(Race $race,Job $job){
        $this->stats = new Stats();
        $this->race = $race;
        $this->job = $job;
        $this->stats->pv = $job->getModifPv();
    }

    function attack(Fighter & $target): void{
        
    }
}

?>