<?php 

abstract class Job {
    // Récupère les props des jobs pour les renvoyer à Stats et/ou Character
    abstract function getModifPv(): int;
    abstract function getModifAtt(): int;
    abstract function getModifDef(): int;
}

?>